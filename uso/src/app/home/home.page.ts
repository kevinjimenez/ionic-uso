import { Component, OnInit } from '@angular/core';
import { CredencialesInterface } from '../interfaces/interfaces.interface';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { AboutPage } from '../about/about.page';
//import { BatteryStatus } from '@ionic-native/battery-status';
import { AndroidPermissions } from '@ionic-native/android-permissions';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {


  ngOnInit(): void {
    this.a()
  }

  credencialesUsuario: CredencialesInterface


  constructor(
    private readonly _loadingController: LoadingController,
    private readonly _navCtrl: NavController,
    public toastController: ToastController,
    //private batteryStatus: BatteryStatus
    //private androidPermissions: AndroidPermissions
  ) { }



  credenciales(evento) {
    console.log(evento)
    if (evento) {
      this.credencialesUsuario = evento
    } else {
      this.credencialesUsuario = undefined
    }
  }

  ingresarUsuario() {
    this._navCtrl.navigateRoot(['/menuConTabs'])
      .then(entro => {
        console.log(entro)
        this.mesajesToast('Bienvenido')
      })
      .catch(error => {
        console.log(error)
        this.mesajesToast('error')
      })
  }

  ingresarInvitado() {
    this._navCtrl.navigateRoot(['/inivitado'])
      .then(entro => {
        console.log(entro)
      })
      .catch(error => {
        console.log(error)
      })
  }

  ingresar() {    
    this._navCtrl.navigateRoot(['/about'])
      .then(entro => {
        console.log(entro)
      })
      .catch(error => {
        console.log(error)
      })
  }

  async mesajesToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,      
      position: 'top',
      closeButtonText: 'Done'
    });
    toast.present();
  }

  a(){
  //   const subscription = this.batteryStatus.onChange().subscribe(status => {
  //     console.log(status.level, status.isPlugged);
  //  });
  }
}
