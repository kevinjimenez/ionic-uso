import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { FomulariosComponent } from './fomularios/fomularios.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        //BrowserAnimationsModule,
        MatInputModule,
        ReactiveFormsModule
    ],
    declarations: [        
        FomulariosComponent
    ],
    exports:[
        FomulariosComponent
    ]

})
export class ComponentesComunModule { }
