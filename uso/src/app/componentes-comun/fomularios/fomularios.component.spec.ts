import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FomulariosComponent } from './fomularios.component';

describe('FomulariosComponent', () => {
  let component: FomulariosComponent;
  let fixture: ComponentFixture<FomulariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FomulariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FomulariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
