import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InivitadoPage } from './inivitado.page';

describe('InivitadoPage', () => {
  let component: InivitadoPage;
  let fixture: ComponentFixture<InivitadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InivitadoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InivitadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
