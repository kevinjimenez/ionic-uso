import { Component, OnInit } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';


@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  slideOpts = {
    effect: 'flip'
  };

  constructor(
    //private androidPermissions: AndroidPermissions
  ) { }

  ngOnInit() {
    //this.androidPermissions.requestPermission()
  }

}
