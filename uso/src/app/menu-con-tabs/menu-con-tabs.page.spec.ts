import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuConTabsPage } from './menu-con-tabs.page';

describe('MenuConTabsPage', () => {
  let component: MenuConTabsPage;
  let fixture: ComponentFixture<MenuConTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuConTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuConTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
