import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-menu-con-tabs',
  templateUrl: './menu-con-tabs.page.html',
  styleUrls: ['./menu-con-tabs.page.scss'],
})
export class MenuConTabsPage implements OnInit {

  constructor(
    public toastController: ToastController
  ) { }

  ngOnInit() {
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      message: 'Click to Close',
      showCloseButton: true,
      position: 'top',
      closeButtonText: 'Done'
    });
    toast.present();
  }

}
