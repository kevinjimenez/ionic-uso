import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },   
  { path: 'inivitado', loadChildren: './inivitado/inivitado.module#InivitadoPageModule' },
  { path: 'menuConTabs', loadChildren: './menu-con-tabs/menu-con-tabs.module#MenuConTabsPageModule' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
