import { Component, OnInit } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { CredencialesInterface } from '../interfaces/credenciales.interface';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

  credencialesUsuario: CredencialesInterface


  constructor(
    private readonly _loadingController: LoadingController,
    private readonly _navCtrl: NavController,
    private readonly _toastController: ToastController,
    private readonly _androidPermissions: AndroidPermissions,
    private readonly _router:Router
  ) { }
  ngOnInit(): void {    
    //this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
  }


  credenciales(evento) {    
    if (evento) {
      this.credencialesUsuario = evento
    } else {
      this.credencialesUsuario = undefined
    }
  }

  ingresarUsuario() {
    this._navCtrl.navigateRoot(['/menu-usuario','menu'])
      .then(entro => {
        console.log(entro)
        this.mesajesToast('Bienvenido Usuario')
      })
      .catch(error => {
        console.log(error)
        this.mesajesToast('error')
      })
  }

  ingresarInvitado() {
    this.mesajesToast('Bienvenido Invitado')
    this._router.navigate(['/menu-usuario','menu'])
  }

  ingresar() {    
    this._navCtrl.navigateRoot(['/about'])
      .then(entro => {
        console.log(entro)
      })
      .catch(error => {
        console.log(error)
      })
  }

  async mesajesToast(mensaje: string) {
    const toast = await this._toastController.create({
      message: mensaje,      
      position: 'middle',    
      duration: 2000
    });
    toast.present();
  }

}
