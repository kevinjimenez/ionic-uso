import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CredencialesInterface } from '../../../interfaces/credenciales.interface';
import { erroresValidaciones } from '../../../constantes/mensajesDeErroresValidaciones/mensajesDeErroresValidaciones';

@Component({
  selector: 'app-fomulario-login',
  templateUrl: './fomulario-login.component.html',
  styleUrls: ['./fomulario-login.component.scss']
})
export class FomularioLoginComponent implements OnInit {

  formularioLogin: FormGroup
  @Input() usuario: string
  @Input() password: string

  @Output() enviarCredenciales: EventEmitter<CredencialesInterface | boolean> = new EventEmitter

  mensajesError = []
  mensajesErrorPassword = []

  constructor(
    private readonly _formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.estructuraFormularioLogin()
    this.obtenerCredenciales()
    this.escuchaCorreo()
    this.escuchaPassword()
  }

  estructuraFormularioLogin() {
    this.formularioLogin = this._formBuilder.group({
      usuario: ['', [Validators.required, Validators.pattern(/[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm)]],
      password: ['', [Validators.required]]
    })
  }

  obtenerCredenciales() {
    this.formularioLogin.valueChanges
      .subscribe(credencialesUsuario => {
        if (this.formularioLogin.valid) {
          this.enviarCredenciales.emit(credencialesUsuario)
        } else {
          this.enviarCredenciales.emit(false)
        }
      })
  }

  validacionCorreo() {

  }

  escuchaCorreo() {
    const correoFormControl = this.formularioLogin.get('usuario');
    correoFormControl
      .valueChanges
      .subscribe(valor => {
        this.setearMensajeCorrreoError(correoFormControl)
      })
  }

  setearMensajeCorrreoError(inputCorreo: AbstractControl) {
    this.mensajesError = []
    if ((inputCorreo.touched || inputCorreo.dirty) && inputCorreo.errors) {
      this.mensajesError = Object
        .keys(inputCorreo.errors)
        .map(tipoDeError => {
          console.log(tipoDeError)
          return erroresValidaciones[tipoDeError]
        })
    }
  }

  escuchaPassword() {
    const passwordFormControl = this.formularioLogin.get('password');
    passwordFormControl
      .valueChanges
      .subscribe(valor => {
        this.setearMensajePassword(passwordFormControl)
      })
  }

  setearMensajePassword(inputPassword: AbstractControl) {
    this.mensajesErrorPassword = []
    if ((inputPassword.touched || inputPassword.dirty) && inputPassword.errors) {
      this.mensajesErrorPassword = Object
        .keys(inputPassword.errors)
        .map(tipoDeError => {
          console.log(tipoDeError)
          return erroresValidaciones[tipoDeError]
        })
    }
  }

  validacionCustomizadaActivacion(valorInputActivacion: AbstractControl): { [atributo: string]: boolean } | null {
        if (valorInputActivacion.value === true || valorInputActivacion.value === false) {
          return null
        } else {
          return { 'soloBoolean': true }
        }
      }


}
