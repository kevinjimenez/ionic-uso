import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FomularioLoginComponent } from './fomulario-login.component';

describe('FomularioLoginComponent', () => {
  let component: FomularioLoginComponent;
  let fixture: ComponentFixture<FomularioLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FomularioLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FomularioLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
