import { NgModule } from "@angular/core";
import { FomularioLoginComponent } from "./formularios/fomulario-login/fomulario-login.component";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { MatInputModule } from '@angular/material/input';

@NgModule({
    declarations:[
        FomularioLoginComponent
    ],    
    imports: [
        CommonModule,
        //BrowserAnimationsModule,
        MatInputModule,
        ReactiveFormsModule
    ],
    providers:[],
    exports:[
        FomularioLoginComponent
    ]
})

export class ComponentesModule{

}