import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuDeIngresoPage } from './menu-de-ingreso.page';
import { PerfilPage } from './perfil/perfil.page';
import { AsistenciaPage } from './asistencia/asistencia.page';
import { CalendarioPage } from './calendario/calendario.page';

const routes: Routes = [  
  {
    path: 'menu',
    component: MenuDeIngresoPage,
    children: [
      {
        path: '',
        redirectTo: 'perfil',
        pathMatch: 'full',
      },
      {
        path: 'perfil',
        outlet: 'perfil',
        component: PerfilPage
      },
      {
        path: 'asistencia',
        outlet: 'asistencia',
        component: AsistenciaPage
      },
      {
        path: 'calendario',
        outlet: 'calendario',
        component: CalendarioPage
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuDeIngresoRoutingModule { }
