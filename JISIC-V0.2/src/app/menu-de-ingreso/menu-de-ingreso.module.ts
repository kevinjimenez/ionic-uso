import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule, MenuController } from '@ionic/angular';
import { MenuDeIngresoPage } from './menu-de-ingreso.page';
import { PerfilPageModule } from './perfil/perfil.module';
import { MenuDeIngresoRoutingModule } from './menu-de-ingreso.routing.module';
import { AsistenciaPageModule } from './asistencia/asistencia.module';
import { CalendarioPageModule } from './calendario/calendario.module';

@NgModule({
  
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,   
    AsistenciaPageModule,
    PerfilPageModule,
    CalendarioPageModule,
    MenuDeIngresoRoutingModule 
  ],
  declarations: [MenuDeIngresoPage]
})
export class MenuDeIngresoPageModule {}
