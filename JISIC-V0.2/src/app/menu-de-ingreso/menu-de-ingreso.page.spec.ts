import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuDeIngresoPage } from './menu-de-ingreso.page';

describe('MenuDeIngresoPage', () => {
  let component: MenuDeIngresoPage;
  let fixture: ComponentFixture<MenuDeIngresoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuDeIngresoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuDeIngresoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
